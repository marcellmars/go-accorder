module main

go 1.16

require (
	github.com/cespare/xxhash v1.1.0 // indirect
	github.com/goccy/go-json v0.7.9 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/karrick/godirwalk v1.16.1 // indirect
)
